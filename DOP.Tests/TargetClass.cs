﻿// Criado por Jone Polvora
// 01 06 2012

namespace DynamicObjectProxy.Tests
{
    public interface IPrivateMethod
    {
        void PrivateMethod();
    }

    public interface IProtectedMethod
    {
        void ProtectedMethod();
    }

    public interface IPublicMethod
    {
        void PublicMethod(int increment);
        int Counter { get; }
    }

    public interface IAllMethods : IPublicMethod, IProtectedMethod, IPrivateMethod
    {
    }

    public class TargetClass : IPublicMethod
    {
        public TargetClass()
        {
            PrivateMethod();
            ProtectedMethod();

        }

        public int Counter { get; private set; }

        void PrivateMethod()
        {
            Counter++;
        }

        protected void ProtectedMethod()
        {
            Counter--;
        }

        public void PublicMethod(int increment)
        {
            Counter = Counter + increment;
        }
    }
}