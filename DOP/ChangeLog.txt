﻿Breaking Changes for next 0.0.4 version

AspectException class:
* Added AspectException class
* Updated usage i ObjectProxy
* [TestMethod] added

Internal Changes for 0.0.4

Option to autoProxy Methods that buildUp instances

* If a method intercepted is a factory method that returns another instance of any type of object,
we can get a handler to that objects and intercept it.